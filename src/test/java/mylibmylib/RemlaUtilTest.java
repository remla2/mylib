package mylibmylib;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import mylib.RemlaUtil;

public class RemlaUtilTest {

	@Test
	public void hostNameNotNullOrEmpty() {
		String actual = RemlaUtil.getHostName();
		assertNotNull(actual);
		assertFalse(actual.isEmpty());
	}

	@Test
	public void versionNotNullOrEmpty() {
		String actual = RemlaUtil.getUtilVersion();
		assertNotNull(actual);
		assertFalse(actual.isEmpty());
	}
}

